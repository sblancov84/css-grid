# CSS Grid

Just to learn CSS Grid

# Links

https://css-tricks.com/snippets/css/complete-guide-grid/#aa-introduction

https://css-tricks.com/quick-whats-the-difference-between-flexbox-and-grid/


# Concepts

## Grid Container

A container, where `display: grid` should be placed. For example:

    <div class="container">
        <div class="item item-1"> </div>
        <div class="item item-2"> </div>
        <div class="item item-3"> </div>
    </div>

## Grid Item

Every element inside the container is an item. For instance:

    <div class="container">
        <div class="item"> </div>
        <div class="item">
            <p class="sub-item"> </p>
        </div>
        <div class="item"> </div>
    </div>

## Grid Line

The dividing lines that make up the structure of the grid. They can be either
vertical or horizontal.

![Grid Line](assets/images/grid_line.png)

## Grid Cell

The space between two adjacent row and adjacent column grid lines. It is a
single "unit" of the grid.

![Grid Cell](assets/images/grid_cell.png)

## Grid Track

The space between two adjacent grid lines. So they are the rows or columns
of the grid.

![Grid Track](assets/images/grid_track.png)

## Grid Area

The total space surrounded by four grid lines. They are like a matrix.

![Grid Area](assets/images/grid_area.png)

# Properties

## Properties for Grid Container

### display

Posible values:
* grid
* inline-grid

### grid-template-rows grid-template-columns

They have two components:
* track-size: length, percentage, fraction of the free space in the grid.
* line-name: an arbitrary name.

If not any name is assigned, lines are named with numbers starting at 1.

For example:

    # Non named lines:
    .container {
        grid-template-columns: 40px 50px auto 50px 40px;
        grid-template-rows: 25% 100px auto;
    }

![Grid Template Rows](assets/images/grid-template-rows_example.png)

    # Named lines:
    .container {
        grid-template-columns: [first] 40px [line2] 50px [line3] auto [col4-start] 50px [five] 40px [end];
        grid-template-rows: [row1-start] 25% [row1-end] 100px [third-line] auto [last-line];
    }

![Grid Template Rows](assets/images/grid-template-rows_named.png)


    # With free space, fr unit:
    .container {
          grid-template-columns: 1fr 1fr 1fr;
    }

### grid-template-areas

Three posible values:
* grid-area-name: The name of a grid area specified with grid-area.
* .: a period signifies an empty area.
* none: no grid areas are defined.

For instance:

    .item-a {
        grid-area: header;
    }
    .item-b {
        grid-area: main;
    }
    .item-c {
        grid-area: sidebar;
    }
    .item-d {
        grid-area: footer;
    }

    .container {
        display: grid;
        grid-template-columns: 50px 50px 50px 50px;
        grid-template-rows: auto;
        grid-template-areas:
            "header header header header"
            "main main . sidebar"
            "footer footer footer footer";
    }

This code will create:

![Grid Template Area](assets/images/grid-template-area_example.png)

### grid-template

It is a shorthand of grid-template-rows, grid-template-columns and
grid-template-areas.

For example:

    .container {
        grid-template:
            [row1-start] "header header header" 25px [row1-end]
            [row2-start] "footer footer footer" 25px [row2-end]
            / auto 50px auto;
    }

is equivalent to:

    .container {
        grid-template-rows: [row1-start] 25px [row1-end row2-start] 25px [row2-end];
        grid-template-columns: auto 50px auto;
        grid-template-areas:
            "header header header"
            "footer footer footer";
    }

### column-gap row-gap

They specifies the size of the grid lines, that is, the width of the gutters
between rows and columns.

    .container {
        grid-template-columns: 100px 50px 100px;
        grid-template-rows: 80px auto 80px;
        column-gap: 10px;
        row-gap: 15px;
    }

![Row/Column gap](assets/images/row-gap_column_gap.png)


### gap

A shorthand for row-gap and column-gap

    .container {
        grid-template-columns: 100px 50px 100px;
        grid-template-rows: 80px auto 80px;
        gap: 15px 10px;
    }

is equivalent to

    .container {
        grid-template-columns: 100px 50px 100px;
        grid-template-rows: 80px auto 80px;
        row-gap: 15px
        column-gap: 10px;
    }

### justify-items

Align grid items along the inline axis. This value applies to all grid items
inside the container.

Possible values:
* start: At the start edge of their cell.
* end: At the end edge of their cell.
* center: In the center of their cell.
* stretch: Fills the hole width of the cell.

    .container {
        justify-items: start;
    }

![Justify Items start](assets/images/justify-items_start.png)

    .container {
        justify-items: end;
    }

![Justify Items end](assets/images/justify-items_end.png)

    .container {
        justify-items: center;
    }

![Justify Items center](assets/images/justify-items_center.png)

    .container {
        justify-items: stretch;
    }

![Justify Items stretch](assets/images/justify-items_stretch.png)

### align-items

Align items along the block axis. This value applies to all items inside the
container.

Possible values:
* stretch: Fills the hole height of the cell.
* start: aligns items to be flush with the start edge of their cell.
* end: align items to be flush with the end edge of their cell.
* center: align items in the center of their cell.
* first baseline: align items along text baseline using as baseline the first line.
* last baseline: align items along text baseline using as baseline the last line.

Examples:

    .container {
        align-items: start;
    }

![Align Items start](assets/images/align-items_start.png)

    .container {
        align-items: end;
    }

![Align Items end](assets/images/align-items_end.png)

    .container {
        align-items: center;
    }

![Align Items center](assets/images/align-items_center.png)

    .container {
        align-items: stretch;
    }

![Align Items stretch](assets/images/align-items_stretch.png)

There are also modifier keywords `safe` and `unsafe`. For instance:

    align-items: safe end;

With safe will try to not hide any data, unsafe do not care about loss data.

### place-items

It is a shorthand for align-items and justify-items.


### justify-content

Set the aligment of the grid within the grid container when the total size of
grid is less than the size of its grid container. This property align the grid
along the inline axis (rows).

Possible values:
* start: aligns the grid to be flush with the start edge of the grid container.
* end: aligns the grid to be flush with the end edge of the grid container
* center: aligns the grid in the center of the grid container.
* stretch: resizes the grid items to allow the grid fill the full height of the
    grid container.
* space-around: places an even amount of space between each grid item, with
    half-sized spaces of the far ends.
* space-between: places an even amount of space between each grid item, with no
    space at the far ends.
* space-evenly: places an even amount of space between each grid item, including
    the far ends.

Examples:

    .container {
        justify-content: start;
    }

![Justify Content start](assets/images/justify-content_start.png)

    .container {
        justify-content: end;
    }

![Justify Content end](assets/images/justify-content_end.png)

    .container {
        justify-content: center;
    }

![Justify Content center](assets/images/justify-content_center.png)

    .container {
        justify-content: stretch;
    }

![Justify Content stretch](assets/images/justify-content_stretch.png)

    .container {
        justify-content: space-around;
    }

![Justify Content space-around](assets/images/justify-content_space-around.png)

    .container {
        justify-content: space-between;
    }

![Justify Content space-between](assets/images/justify-content_space-between.png)

    .container {
        justify-content: space-evenly;
    }

![Justify Content space-evenly](assets/images/justify-content_space-evenly.png)


### align-content



Examples:

    .container {
        align-content: start;
    }

![Align Content start](assets/images/align-content_start.png)

    .container {
        align-content: end;
    }

![Align Content end](assets/images/align-content_end.png)

    .container {
        align-content: center;
    }

![Align Content center](assets/images/align-content_center.png)

    .container {
        align-content: stretch;
    }

![Align Content stretch](assets/images/align-content_stretch.png)

    .container {
        align-content: space-around;
    }

![Align Content space-around](assets/images/align-content_space-around.png)

    .container {
        align-content: space-between;
    }

![Align Content space-between](assets/images/align-content_space-between.png)

    .container {
        align-content: space-evenly;
    }

![Align Content space-evenly](assets/images/align-content_space-evenly.png)

### place-content

A shorthand for align-content and justify-content, in this order.

### grid-auto-columns grid-auto-rows

Specifies the size of any auto-generated grid tracks (implecit grid tracks).

Example:

    .container {
        grid-template-columns: 60px 60px;
        grid-template-rows: 90px 90px;
    }
    .item-a {
        grid-column: 1 / 2;
        grid-row: 2 / 3;
    }
    .item-b {
        grid-column: 5 / 6;
        grid-row: 2 / 3;
    }

![Grid Auto columns/rows](assets/images/grid-auto-columns_rows.png)

### grid-auto-flow

Controls how the auto-placement algorithm works.

Possible Values:
* row (defaul): Add new rows as necessary.
* column: Add new columns as necessary.
* dense: Attempt to fill holes earlier in the grid if smaller items come up
    later.

Dense might cause items appear out of order.

Example:

    <section class="container">
        <div class="item-a">item-a</div>
        <div class="item-b">item-b</div>
        <div class="item-c">item-c</div>
        <div class="item-d">item-d</div>
        <div class="item-e">item-e</div>
    </section>

    .container {
        display: grid;
        grid-template-columns: 60px 60px 60px 60px 60px;
        grid-template-rows: 30px 30px;
        grid-auto-flow: row;
    }
    .item-a {
        grid-column: 1;
        grid-row: 1 / 3;
    }
    .item-e {
        grid-column: 5;
        grid-row: 1 / 3;
    }

TODO: Add image (saved on captures)

Other example:

    .container {
        display: grid;
        grid-template-columns: 60px 60px 60px 60px 60px;
        grid-template-rows: 30px 30px;
        grid-auto-flow: column;
    }

TODO: Add image (saved on captures)

## Properties for Grid Items
